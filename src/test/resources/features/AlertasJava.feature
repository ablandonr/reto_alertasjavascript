#Author: ablandonr@choucairtesting.com
@tag
Feature: Alertas Java Script

  @Caso1
  Scenario: Ingresar a la pagina y aprender a manejar alertas tipo Java Script
    Given que el usuario ingresa a la página de HeroKuapp
    When selecciona el link javascript_alerts
    And interactúa con los controles que hay en esta pantalla
    Then Aprende a manejar Alertas tipo JavaScript y en Result aparece You entered: Hola mundo
