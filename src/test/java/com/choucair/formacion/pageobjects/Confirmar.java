package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Confirmar extends PageObject{

	//Verificar mensaje ingresado
	@FindBy (id="result")
	public WebElementFacade txtConfirmacion;
	
	public void palabra(String clave) {
		assertThat(txtConfirmacion.containsText(clave),is(true));
	}
	

}
