package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class Interactuando extends PageObject{
	
	//Dar click en JS Alert
	@FindBy (xpath="//*[@onclick='jsAlert()']")
	public WebElementFacade btnAlertaJS;
	
	//Dar click en JS Confirm
	@FindBy (xpath="//*[@onclick='jsConfirm()']")
	public WebElementFacade btnConfirmarJS;
	
	//Dar click en JS Prompt
	@FindBy (xpath="//*[@onclick='jsPrompt()']")
	public WebElementFacade btnRapidoJS;
	
	public void conAlertaJS() {
		btnAlertaJS.click();
	}
	public void conConfirmacionJS() {
		btnConfirmarJS.click();
	}
	public void conRapidoJS() {
		btnRapidoJS.click();
	}

}
