package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SeleccionarAlertas extends PageObject{
	
	//Dar click en Alertas JavaScript 
	@FindBy (xpath="//*[@href=\'/javascript_alerts\']")
	public WebElementFacade btnAlertaJava;
	
	public void deJavaStript() {
		btnAlertaJava.click();
	}
}
