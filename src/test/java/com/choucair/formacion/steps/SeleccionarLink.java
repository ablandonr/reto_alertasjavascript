package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.SeleccionarAlertas;

import net.thucydides.core.annotations.Step;

public class SeleccionarLink {
	SeleccionarAlertas seleccionarAlertas;
	
	@Step
	public void abrirAlertas() {
		seleccionarAlertas.deJavaStript();
	}

}
