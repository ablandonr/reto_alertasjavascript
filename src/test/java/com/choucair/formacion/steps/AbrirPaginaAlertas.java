package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.PaginaHeroKuapp;

import net.thucydides.core.annotations.Step;

public class AbrirPaginaAlertas {
	
	static PaginaHeroKuapp paginaHeroKuapp;
	
	@Step
	public void AbrirLaPagina () {
		paginaHeroKuapp.open();
	}

}
