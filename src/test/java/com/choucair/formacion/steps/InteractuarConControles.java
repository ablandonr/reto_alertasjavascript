package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.Interactuando;

import net.thucydides.core.annotations.Step;

public class InteractuarConControles {

	Interactuando interactuando;
	@Step
	public void enPantalla() throws InterruptedException {
		interactuando.conAlertaJS();
		interactuando.getAlert().accept();
		Thread.sleep(4000);
		interactuando.conConfirmacionJS();
		interactuando.getAlert().dismiss();
		Thread.sleep(4000);
		interactuando.conRapidoJS();
		interactuando.getAlert().sendKeys("Hola mundo");
		Thread.sleep(4000);
		interactuando.getAlert().accept();
		Thread.sleep(4000);
		
	}

}
