package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.Confirmar;

import net.thucydides.core.annotations.Step;

public class VerificarMensaje {
	
	Confirmar confirmar;
	
	@Step
	public void conPalabra(String clave) {
		confirmar.palabra(clave);
	}

}
