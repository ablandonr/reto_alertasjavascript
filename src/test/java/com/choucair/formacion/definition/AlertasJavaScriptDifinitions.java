package com.choucair.formacion.definition;

import com.choucair.formacion.steps.AbrirPaginaAlertas;
import com.choucair.formacion.steps.InteractuarConControles;
import com.choucair.formacion.steps.SeleccionarLink;
import com.choucair.formacion.steps.VerificarMensaje;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AlertasJavaScriptDifinitions {
	
	@Steps
	AbrirPaginaAlertas abrirPaginaAlertas;
	@Steps
	SeleccionarLink seleccionarLink;
	@Steps
	InteractuarConControles interactuarConControles;
	@Steps
	VerificarMensaje verificarMensaje;
	
	@Given("^que el usuario ingresa a la página de HeroKuapp$")
	public void que_el_usuario_ingresa_a_la_página_de_HeroKuapp() throws Throwable {
		abrirPaginaAlertas.AbrirLaPagina();
	}

	@When("^selecciona el link javascript_alerts$")
	public void selecciona_el_link_javascript_alerts() throws Throwable {
		seleccionarLink.abrirAlertas();
	}

	@When("^interactúa con los controles que hay en esta pantalla$")
	public void interactúa_con_los_controles_que_hay_en_esta_pantalla() throws Throwable {
		interactuarConControles.enPantalla();
	}

	@Then("^Aprende a manejar Alertas tipo JavaScript y en Result aparece (.*)$")
	public void aprende_a_manejar_Alertas_tipo_JavaScript_y_en_Result_aparece(String clave) throws Throwable {
		verificarMensaje.conPalabra(clave);
	}

}
